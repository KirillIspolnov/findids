package net.kivitechnologies.BrainWork.MyIDFinder;

import org.kllbff.obfuscator.JSource;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Простой тест для нашей программы
 * Имеется файл src.java, находящийся в корневой папке проекта
 * 
 * @author Кирилл Испольнов, 16ит18К
 */
public class Tester {
    public static void main(String[] args) {
        String source = read("JSource.java");
        
        JSource jSource = new JSource();
        jSource.setSource(source);
        
        ArrayList<String> ids = jSource.getIdentifiers(true, true);
        System.out.printf("Я нашёл %d различных идентификаторов (за исключением клюевых слов и повторяющих идентификаторов)\n", ids.size());
        write("ids.txt", ids);
        
        String obfuscated = jSource.obfuscate();
        System.out.printf("Обфусцирование кода позволило сократить объём кода на %d символов\n", source.length() - obfuscated.length());
        write("JSource.obf.java", obfuscated); 
    }

    /**
     * Считывает содержимое файла, находящегося по пути path
     * 
     * @param path путь к файлу
     * @return содержимое текстового файла
     */
    private static String read(String path) {
        StringBuilder source = new StringBuilder();
        
        BufferedReader fr = null;
        
        try{
            fr = new BufferedReader(new FileReader(path));
            String s;
            while((s = fr.readLine()) != null) {
                source.append(s).append("\n");
            }
        } catch (IOException ioe) {
            System.err.println(ioe);
        }
        
        try {
            fr.close();
        } catch (NullPointerException | IOException ex) {
            System.err.println(ex);
        }
        
        return source.toString();
    }
    
    /**
     * Записывает найденные идентификаторы в файл, находящийся по пути path
     * 
     * @param path путь к файлу
     * @param data данные для записи 
     */
    private static void write(String path, Object data) {
        BufferedWriter fw = null;
        
        try {
            fw = new BufferedWriter(new FileWriter(path));
            
            if(data instanceof ArrayList) {
                ArrayList<String> ids = (ArrayList<String>)data; 
                for(String id : ids) {
                    fw.write(id);
                    fw.write("\n");
                    fw.flush();
                }
            } else {
                fw.write(data.toString());
            }
        } catch(IOException ioe) {
            System.err.println(ioe);
        }
        
        try {
            fw.close();
        } catch (NullPointerException | IOException ex) {
            System.err.println(ex);
        }
    }
}