package net.kivitechnologies.BrainWork.MyIDFinder;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FIDS {
    public static ArrayList<String> readSourceWithBaBaBah() {
        ArrayList<String> lines = new ArrayList<>();
        BufferedReader reader = null;
        
        Pattern pattern = Pattern.compile("\".*\"");
        Matcher matcher = pattern.matcher("");
        
        try {
            reader = new BufferedReader(new FileReader("src.java"));
            String line;
            while((line = reader.readLine()) != null) {
                line = line.replaceAll("\\\\\"", "");
                
                matcher.reset(line);
                while(matcher.find()) {
                    line = line.replace(matcher.group(), "");
                }
                
                lines.add(line);
            }
        } catch (IOException ioe) {
            System.err.println("Траблы у вас, поцаны: " + ioe.toString());
        }
        
        try {
            reader.close();
        } catch(IOException | NullPointerException e) {
            System.err.println("Траблы у вас, поцаны: " + e.toString());
        }
        
        return lines;
    }
    
    public static void writeIdentifiers(ArrayList<String> identifiers) {
        FileWriter writer = null;
        
        try {
            writer = new FileWriter("ids.txt");
            for(String id : identifiers) {
                writer.write(id);
                writer.write("\n");
            }
        } catch (IOException ioe) {
            System.err.println("Траблы у вас, поцаны: " + ioe.toString());
        }
        
        try {
            writer.close();
        } catch(IOException | NullPointerException e) {
            System.err.println("Траблы у вас, поцаны: " + e.toString());
        }
    }

    public static final boolean IGNORE_KEYWORDS = true, IGNORE_FOUNDED = true;
    public static final String[] KEYWORDS = {
        "byte", "short", "int", "long", "char", "float", "double", "boolean",
        "if", "else", "switch", "case", "default", "while", "do", "break", "continue", "for",
        "try", "catch", "finally", "throw", "throws", 
        "private", "protected", "public",
        "import", "package", "class", "interface", "extends", "implements", "static", "final", "void", "abstract", "native",
        "new", "return", "this", "super",
        "synchronized", "volatile",
        "const", "goto", "enum", "instanceof", "assert", "transient", "strictfp"
    };
    
    public static boolean isKeyWord(String id) {
        for(String keyword : KEYWORDS) {
            if(keyword.equals(id))
                return true;
        }
        
        return false;
    }
    
    public static ArrayList<String> findIdentifiers(ArrayList<String> lines) {
        ArrayList<String> identifiers = new ArrayList<>();
        Pattern pattern = Pattern.compile("\\b[A-Za-zА-Яа-я_]\\w*\\b");
        Matcher matcher = pattern.matcher("");
        
        String id;
        for(String line : lines) {
            matcher.reset(line);
            while(matcher.find()) {
                id = matcher.group();
                if(IGNORE_KEYWORDS && isKeyWord(id) || (IGNORE_FOUNDED && identifiers.contains(id)))
                    continue;
                
                identifiers.add(id);
            }
        };
        return identifiers;
    }
    
    public static void main(String[] args) throws Throwable {
        ArrayList<String> lines = readSourceWithBaBaBah();
        ArrayList<String> identifiers = findIdentifiers(lines);
        writeIdentifiers(identifiers);
    }
}
